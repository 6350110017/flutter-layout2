import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Layout',
      theme: ThemeData(

        primarySwatch: Colors.red,
      ),
      home: const MyHomePage(title: 'My fisrt Layout'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);


  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {


  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(

        title: Text(widget.title),
      ),
      body: Container(
        color:Colors.redAccent,
        constraints: BoxConstraints.expand(),
        child: SingleChildScrollView(
          child: Column(
            children: [
              CircleAvatar(
                backgroundColor: Colors.white,
                radius: 80,
                child: Icon(
                  Icons.perm_identity,
                  size: 80,
                ),
              ),
              Row(
                mainAxisAlignment:MainAxisAlignment.spaceAround,
                children: [
                  Text('Text 1',style:TextStyle(
                      height: 5,
                      fontSize: 24,
                      fontWeight: FontWeight.bold
                  )
                    ),
                  Text('Text 2',style:TextStyle(
                      height: 5,
                      fontSize: 24,
                      fontWeight: FontWeight.bold
                  )
                    ),
                  Text('Text 3',
                      style:TextStyle(
                      height: 5,
                      fontSize: 24,
                      fontWeight: FontWeight.bold
                  )
                    ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(bottom:15),
                child: Text('Text',style: TextStyle(
                    height:3,
                    fontSize: 24,
                  fontWeight:FontWeight.bold
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: TextField(
                  decoration: InputDecoration(
                    hintText:'Username',
                    fillColor: Colors.white,
                    filled: true
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 15,left: 15,bottom: 30),
                child: TextField(decoration: InputDecoration(
                      hintText: 'Password',
                  fillColor: Colors.white,
                  filled: true,
                  )
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary:Colors.blue,
                        onPrimary: Colors.white,
                      padding: EdgeInsets.symmetric(horizontal: 30,vertical: 15),
                      textStyle: TextStyle(fontSize: 20),
                    ),
                    onPressed: ()=>print('Okay1'),
                    child: Text('Cannel'),
                  ),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary:Colors.blue,
                      onPrimary: Colors.white,
                      padding: EdgeInsets.symmetric(horizontal: 30,vertical: 15),
                      textStyle: TextStyle(fontSize: 20),
                    ),
                    onPressed: ()=>print('Okay1'),
                    child: Text('Login'),
                  ),
                ],
              ),


            ],
          ),
        ),
      ),
    );
  }
}
